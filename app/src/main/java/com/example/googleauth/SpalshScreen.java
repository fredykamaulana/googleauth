package com.example.googleauth;

import android.content.Intent;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

public class SpalshScreen extends AwesomeSplash {
    @Override
    public void initSplash(ConfigSplash configSplash) {
        //costomize circular reveal
        configSplash.setBackgroundColor(R.color.fillColor); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(1000); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM);

        //costomize logo
        configSplash.setLogoSplash(R.mipmap.ic_launcher); //or any other drawable
        configSplash.setOriginalHeight(500);
        configSplash.setOriginalWidth(500);
        configSplash.setAnimLogoSplashDuration(1000); //int ms
        configSplash.setAnimLogoSplashTechnique(Techniques.RotateIn); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)
        configSplash.setTitleSplash("Firebase Auth");


    }

    @Override
    public void animationsFinished() {
        startActivity(new Intent(SpalshScreen.this, MainActivity.class));
        finish();

    }
}
